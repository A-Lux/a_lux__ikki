<div class="component-slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="slider__h2">
                    НАШ INSTAGRAM
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="glide_sub">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            <li class="glide__slide">
                                <img src="images/slider-sub-1.png" alt="slider-img-1">
                            </li>
                            <li class="glide__slide">
                                <img src="images/slider-sub-2.png" alt="slider-img-2">
                            </li>
                            <li class="glide__slide">
                                <img src="images/slider-sub-3.png" alt="slider-img-3">
                            </li>
                            <li class="glide__slide">
                                <img src="images/slider-sub-4.png" alt="slider-img-4">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>