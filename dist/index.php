<?php require_once("header.php"); ?>

<main class="index">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 slider-top">
                <a class="slider-top__btn">Подписаться</a>
                <div class="glide">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            <li class="glide__slide"><img src="images/index-slider1.png" alt=""></li>
                            <li class="glide__slide"><img src="images/index-slider1.png" alt=""></li>
                            <li class="glide__slide"><img src="images/index-slider1.png" alt=""></li>
                        </ul>
                    </div>
                    <div class="glide__arrows" data-glide-el="controls">
                        <button class="glide__arrow glide__arrow--left" data-glide-dir="<"></button>
                        <button class="glide__arrow glide__arrow--right" data-glide-dir=">"></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h2 class="main-gallery__h2">КАТАЛОГ</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6   main-gallery">
                <div class="column">
                    <div class="column__item">
                        <img src="images/gallery-main-1.png" alt="">
                    </div>
                    <div class="column__item">
                        <img src="images/gallery-main-2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-6   main-gallery">
                <div class="column">
                    <div class="column__item">
                        <img src="images/gallery-main-3.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        
    </div>
  
</main>
<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>