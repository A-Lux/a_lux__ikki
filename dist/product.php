<?php require_once("header.php"); ?>
<div class="product-wrapper">
    <div class="basket-product">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 pl-0 pr-0" style="position: static;">
                    <div class="basket__menu">
                        <div class="basket__menu-header">
                            <h1>Каталог</h1>
                            <button type="button" class="close__btn">
                                <span>×</span>
                            </button>
                        </div>
                        <div class="basket__menu-body">
                            <img src="images/basket_menu-img.png" alt="">
                            <div class="product__description">
                                <h2>Пиджак в полоску sandsend</h2>
                                <p>6/XXS</p>
                            </div>
                            <div class="product__description">
                                <div class="product__amount">
                                    <button type="button" class="decrease">
                                        -
                                    </button>
                                    <p class="">1</p>
                                    <button type="button" class="increase">
                                        +
                                    </button>
                                </div>
                                <p>66 900 KZT</p>
                            </div>
                        </div>
                        <div class="basket__menu-footer">
                            <h1>ИТОГО: 66 900 KZT</h1>
                            <button type="button" class="payForOrder">Оплатить заказ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="notification-row">
        <div class="container">
            <div class="row">
                <div class="title">
                    <h2 class="title-h2">YOUR SHOPPING CART QUALIFIES FOR FREE SHIPPING WITHIN THE UK</h2>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("breadCrumbs.php"); ?>
    <div class="product-inner">
        <div class="container">
            <div class="row">
                <div class="col-sm-1 px-0">
                    <div class="product-inner__wrap-slider">
                        <div class="slider slider-nav ">
                            <div><img src="images/product-inner1.png" alt=""></div>
                            <div><img src="images/product-inner1.png" alt=""></div>
                            <div><img src="images/product-inner1.png" alt=""></div>
                            <div><img src="images/product-inner1.png" alt=""></div>
                            <div><img src="images/product-inner1.png" alt=""></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="slider  slider-for">
                        <div><img src="images/product-inner1.png" alt=""></div>
                        <div><img src="images/product-inner1.png" alt=""></div>
                        <div><img src="images/product-inner1.png" alt=""></div>
                        <div><img src="images/product-inner1.png" alt=""></div>
                        <div><img src="images/product-inner1.png" alt=""></div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <h2 class="product-inner__title">пиджак в полоску Sandsend, <br>
                        бежевый</h2>
                    <div class="status-wrap">
                        <div class="catalog__body-container__display__item__status__label--NEW">
                            NEW
                        </div>
                        <div class="catalog__body-container__display__item__status__label--SALE">
                            SALE
                        </div>
                    </div>
                    <div class="parameters-wrap">
                        <div class="parameters-wrap__select-wrap">
                            <h5>размер</h5>
                            <select class="js-example-basic-single" name="state">
                            </select>
                        </div>
                        <div class="parameters-wrap__select-wrap">

                            <button class="btn catalog__body-container__filter__row__select2 " type="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                по цвету
                            </button>
                            <div class="dropdown-menu dropdown-menu-color">
                                <div class="container">
                                    <div class="row">
                                        <?php
                                            for($i = 1; $i < 13; $i++){
                                                require("catalogColorCircleFilter.component.php");
                                            }
                                            ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="parameters-wrap__select-wrap">
                            <h5>материал</h5>
                            <span>
                                ЛЁН
                            </span>
                        </div>
                        <div class="parameters-wrap__select-wrap">
                            <h5>артикуль</h5>
                            <span>
                                11077
                            </span>
                        </div>
                    </div>
                    <div class="product-inner__description">
                        <p>Long line and lightweight linen jacket with a single button fastening and two pockets. A UK
                            size
                            6 - 8 can expect a slouchy fit whilst a UK size 14 will wear more fitted.

                            One Size: Length: 73cm, Across Width (Bust): 51cm, Sleeve: 45cm.

                            100% Linen.</p>


                    </div>
                    <div class="product-inner__price">
                        <span>22 300 KZT</span>
                    </div>
                    <div class="product-inner__btn">
                        <a href="javascript:void(0)" class="addToBasket">
                            В корзину
                        </a>
                        <a href="">
                            В избранное
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-sliders">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 px-0">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="similar-products" data-toggle="pill" href="#pills-home"
                                    role="tab" aria-controls="pills-home" aria-selected="true">Похожии товары</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="together-products" data-toggle="pill" href="#pills-profile"
                                    role="tab" aria-controls="pills-profile" aria-selected="false">Готовый образ</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                aria-labelledby="similar-products">

                                <div class="glideSimilar">
                                    <div data-glide-el="track" class="glide__track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide"><?php require("product-item.php"); ?></li>
                                            <li class="glide__slide"><?php require("product-item.php"); ?></li>
                                            <li class="glide__slide"><?php require("product-item.php"); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                aria-labelledby="together-products">

                                <div class="glideTogether">
                                    <div data-glide-el="track" class="glide__track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide"><?php require("product-item.php"); ?></li>
                                            <li class="glide__slide"><?php require("product-item.php"); ?></li>
                                            <li class="glide__slide"><?php require("product-item.php"); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>