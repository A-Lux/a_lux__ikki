<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
        
    <title>Document</title>
</head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <img src="./images/logo.png" alt="IKKI" id="ikkiLogo">
                </div>
                <div class="col-sm-7">
                    <ul class="sf-menu" id="example">
                        <li class="current nav-root"> <a href="#" class="nav-root">Новинки</a>
                            <!-- <ul>
                                <li class="current"> <a href="#">menu item</a>
                                    <ul>
                                        <li class="current"><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                    </ul>
                                </li>
                                <li> <a href="#">Каталог</a>
                                    <ul>
                                        <li><a href="#">ОДЕЖДА</a></li>
                                        <li><a href="#">ОБУВЬ</a></li>
                                        <li><a href="#">АКСЕССУАРЫ</a></li>
                                    </ul>
                                </li>
                                <li> 
                                    <ul>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        <li class="nav-root"> <a href="./catalog.php" class="nav-root">Каталог</a>
                            <ul class="nav--bg-pink">

                                <li><a href="">ОДЕЖДА</a>
                                    <ul class="nav--bg-pink">
                                        <li><a href="#">Пиджаки и костюмы</a></li>
                                        <li><a href="#">Платья и сарафаны</a></li>
                                        <li><a href="#">Спортивные костюмы</a></li>
                                        <li><a href="#">Толстовки и олимпийки</a></li>
                                        <li><a href="#">Топы и майки</a></li>
                                        <li><a href="#">Туники</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">ОБУВЬ</a></li>
                                <li><a href="#">АКСЕССУАРЫ</a></li>

                            </ul>
                        </li>
                        <li class="nav-root"> <a href="#" class="nav-root">SALE</a>
                            <!-- <ul>
                                <li> <a href="#">menu item</a>
                                    <ul>
                                        <li><a href="#">short</a></li>
                                        <li><a href="#">short</a></li>
                                        <li><a href="#">short</a></li>
                                        <li><a href="#">short</a></li>
                                        <li><a href="#">short</a></li>
                                    </ul>
                                </li>
                                <li> <a href="#">menu item</a>
                                    <ul>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                    </ul>
                                </li>
                                <li> <a href="#">menu item</a>
                                    <ul>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                        <li><a href="#">menu item</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </li>

                    </ul>
                </div>
                <div class="col-sm-3">
                    <a href="" class="user-cabinet"></a>
                    <div class="user-cabinet__intrface">
                        <div class="user-cabinet__intrface_item">
                            <p>Личный кабинет</p>
                            <a href="">
                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" width="24px"
                                    height="20px" viewBox="0 0 245.5 300.64">
                                    <title>person</title>
                                    <path
                                        d="M281.18,301H39.68v-4l-4,0v-4h.12a123.18,123.18,0,0,1,70.27-105.49l1.5-.68c3.3-1.51,6.72-2.89,10.17-4.1l1-.37c2-.71,3.9-1.3,5.77-1.83,2.38-.7,4.86-1.34,7.35-1.9,1.32-.28,2.59-.53,3.9-.78l.52-.09c.88-.17,1.75-.33,2.64-.47l.42-.07c2.31-.37,4.12-.61,5.79-.79l4.29-.38a117.25,117.25,0,0,1,17.78,0l4.25.35c1,.12,2.08.26,3.35.44l1.48.21c1.53.23,3.09.49,4.78.81l.72.13c.9.17,1.79.34,2.64.52,2.44.54,5,1.19,7.77,2,1.38.4,3.19.94,5,1.53,2,.67,4.07,1.43,6.16,2.25,1.9.75,3.85,1.58,5.77,2.46l.33.15a123.32,123.32,0,0,1,71.66,110.05ZM43.8,293H273a115.34,115.34,0,0,0-66.88-98.83l-.34-.16c-1.77-.81-3.58-1.58-5.36-2.27-2-.78-3.89-1.49-5.75-2.11-1.66-.55-3.38-1.06-4.68-1.44-2.62-.76-5-1.37-7.23-1.86-.75-.16-1.58-.32-2.42-.47l-.72-.14c-1.59-.3-3-.54-4.46-.75l-1.46-.21c-1.16-.16-2.12-.29-3.07-.4l-4-.34a109.62,109.62,0,0,0-16.51,0l-4.15.36c-1.46.16-3.15.39-5.25.72l-.41.07c-.9.14-1.71.29-2.51.44l-.52.1c-1.24.23-2.45.47-3.66.73-2.27.51-4.59,1.11-6.83,1.77-1.76.5-3.49,1-5.33,1.68l-1,.37c-3,1.06-6,2.25-8.9,3.55l-.23.23-1.93.75A115.17,115.17,0,0,0,43.8,293Z"
                                        transform="translate(-35.68 -0.32)" />
                                    <path
                                        d="M159.23,163.75h-.75a81.21,81.21,0,0,1-24.26-3.69c-.59-.18-1.17-.4-1.75-.62l-3.2-1.18c-.81-.28-1.65-.58-2.5-.93A81.58,81.58,0,0,1,76.87,82c0-1.28.09-2.48.17-3.65l.1-1.52A81.75,81.75,0,0,1,158.58.32c45,0,81.71,36.36,81.75,81l0,.89A81.82,81.82,0,0,1,210.49,145l-.13.1L207,147.72c-.83.67-1.66,1.33-2.7,2.05-1.89,1.29-3.84,2.41-5.68,3.45l-1.16.7c-1.67.9-3.37,1.67-5.11,2.45l-2.07,1c-1.12.46-2.26.86-3.36,1.24l-2.81,1c-.46.17-.92.34-1.37.48-.81.25-1.63.45-2.41.64l-3.61.92c-.58.16-1.15.31-1.67.42-1,.2-1.94.33-2.85.46l-3,.45c-.59.11-1.37.24-2.28.33C164.38,163.58,161.8,163.73,159.23,163.75ZM158.58,8.32A73.73,73.73,0,0,0,85.13,77.37L85,79C84.94,80,84.87,81,84.87,82a73.59,73.59,0,0,0,45,67.91c.62.27,1.31.51,2,.76l3.42,1.26c.43.16.85.32,1.28.46a73.09,73.09,0,0,0,21.89,3.32h.73c2.23,0,4.55-.15,6.81-.39.64-.06,1.23-.16,1.81-.26l3.22-.49c.83-.11,1.62-.22,2.36-.37.32-.06.74-.18,1.17-.3l3.8-1c.7-.17,1.37-.33,2-.52.3-.09.58-.2.86-.31l3-1.09c1-.35,1.95-.68,2.83-1l1.92-.93c1.58-.7,3.12-1.39,4.54-2.15l1.05-.64c1.7-1,3.51-2,5.15-3.12.8-.55,1.45-1.07,2.23-1.7l.09-.08,3.36-2.56a73.81,73.81,0,0,0,26.85-56.71l0-.86C232.23,41,199.17,8.32,158.58,8.32Z"
                                        transform="translate(-35.68 -0.32)" />
                                </svg>
                            </a>
                        </div>
                        <div class="user-cabinet__intrface_item">
                            <p>Корзина</p>
                            <a href="">
                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" width="24px"
                                    height="20px" viewBox="0 0 24.63 32.92">

                                    <title>bag</title>
                                    <path class="cls-1" d="M11.6,10.13V5.22a4.9,4.9,0,0,1,9.8,0v4.91"
                                        transform="translate(-4.19 -0.08)" />
                                    <path
                                        d="M28.31,33H4.69a.51.51,0,0,1-.36-.15.48.48,0,0,1-.14-.37L5.31,8.85a.5.5,0,0,1,.5-.47H27.19a.5.5,0,0,1,.5.47l1.12,23.63a.45.45,0,0,1-.14.36A.48.48,0,0,1,28.31,33ZM4.74,32.48H28.26L27.14,8.9H5.86Z"
                                        transform="translate(-4.19 -0.08)" />
                                </svg>
                            </a>
                        </div>
                        <div class="user-cabinet__intrface_item">
                            <p>Избранное</p>
                            <a href="">
                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" width="24px"
                                    height="20px" viewBox="0 0 449.4 394.85">
                                    <title>fav</title>
                                    <path
                                        d="M245.43,442.55a12.11,12.11,0,0,1-8.41-3.27l-180-180A123.86,123.86,0,1,1,232.13,84.07l7.4,7.4a8.22,8.22,0,0,0,5.87,2.43,8.31,8.31,0,0,0,3.15-.63,8.18,8.18,0,0,0,2.72-1.8l7.2-7.2A123.86,123.86,0,1,1,433.63,259.43l-179.8,179.8A11.88,11.88,0,0,1,245.43,442.55ZM144.4,55.7A115.85,115.85,0,0,0,62.63,253.57l180,180a4.32,4.32,0,0,0,5.54,0L428,253.77A115.85,115.85,0,1,0,264.13,89.93l-7.2,7.2a16.51,16.51,0,0,1-11.53,4.77,16.19,16.19,0,0,1-11.53-4.77l-7.4-7.4A115.33,115.33,0,0,0,144.4,55.7Z"
                                        transform="translate(-20.6 -47.7)" />
                                </svg>
                            </a>
                        </div>
                        <div class="user-cabinet__intrface_item">
                            <p>Поиск</p>
                            <a href="">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="24px"
                                    height="20px" viewBox="0 0 512 512">
                                    <title>search</title>
                                    <path
                                        d="M508.88,493.79,353.09,338a201.62,201.62,0,0,0,52.24-135.34C405.33,90.92,314.42,0,202.67,0S0,90.92,0,202.67,90.92,405.33,202.67,405.33A201.62,201.62,0,0,0,338,353.09L493.79,508.88a10.67,10.67,0,1,0,15.09-15.09ZM202.67,390C99.38,390,15.33,306,15.33,202.67S99.38,15.33,202.67,15.33,390,99.37,390,202.67,306,390,202.67,390Z" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
