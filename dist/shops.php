<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>

<div class="shops">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="shops__title">
                    Магазины
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 pr-0">
                <div class="shops__map-container">

                </div>
            </div>
            <div class="col-sm-4 pl-0">
                <div class="shops__column--wrapper">
                    <div class="shops__column--wrapper__item__title">
                        <h2>
                            Найти магазин
                        </h2>
                        <input type="text" placeholder="Найти магазин">
                    </div>
                    <div class="shops__column--wrapper__item">
                        <ul>
                            <li>Тц Планета</li>
                        </ul>
                        <p>Актау, ул. Левобережная 17, 6824685
                        </p>
                        <div class="shops__column--wrapper__item-flex">
                            <span>8 800 321 50 14 </span>
                            <span> +7 (961) 525 79 31</span>
                        </div>
                    </div>
                    <div class="shops__column--wrapper__item">
                        <ul>
                            <li>Тц Медиаплаза</li>
                        </ul>
                        <p>Актау, ул. Левобережная 17, 6824685
                        </p>
                        <div class="shops__column--wrapper__item-flex">
                            <span>8 800 321 50 14 </span>
                            <span> +7 (961) 525 79 31</span>
                        </div>
                    </div>
                    <div class="shops__column--wrapper__item">
                        <ul>
                            <li>Тц Галактика</li>
                        </ul>
                        <p>Актау, ул. Левобережная 17, 6824685
                        </p>
                        <div class="shops__column--wrapper__item-flex">
                            <span>8 800 321 50 14 </span>
                            <span> +7 (961) 525 79 31</span>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>