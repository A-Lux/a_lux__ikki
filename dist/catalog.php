<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>
<div class="catalog">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="catalog__title--wrap">
                    <h3 class="catalog__title">Каталог</h3>

                    <div class="catalog__quantity">
                        <p>Показывать по:</p>
                        <span class="quantity-target">50</span>
                        <span class="quantity-target">100</span>
                        <span class="quantity-target">200</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="catalog__body-container_collapse">
                    <div class="accordion" id="accordionExample">
                        <div class="catalog__card">
                            <div class="catalog__card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn catalog__btn-link" type="button" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        ОДЕЖДА
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="catalog__card-body">
                                    <ul>
                                        <li>Блузы и рубашки</li>
                                        <li>Боди</li>
                                        <li>Брюки</li>
                                        <li>Верхняя одежда</li>
                                        <li>Джемперы, свитеры</li>
                                        <li>Джинсы</li>
                                        <li>Домашняя одежда</li>
                                        <li>Комбинезоны</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="catalog__card">
                            <div class="catalog__card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn catalog__btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        ОБУВЬ
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordionExample">
                                <div class="catalog__card-body">
                                    <ul>
                                        <li>Блузы и рубашки</li>
                                        <li>Боди</li>
                                        <li>Брюки</li>
                                        <li>Верхняя одежда</li>
                                        <li>Джемперы, свитеры</li>
                                        <li>Джинсы</li>
                                        <li>Домашняя одежда</li>
                                        <li>Комбинезоны</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="catalog__card">
                            <div class="catalog__card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn catalog__btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        АКСЕССУАРЫ
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="catalog__card-body">
                                    <ul>
                                        <li>Блузы и рубашки</li>
                                        <li>Боди</li>
                                        <li>Брюки</li>
                                        <li>Верхняя одежда</li>
                                        <li>Джемперы, свитеры</li>
                                        <li>Джинсы</li>
                                        <li>Домашняя одежда</li>
                                        <li>Комбинезоны</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="catalog__body-container--flex">
                    <div class="catalog__body-container__filter">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="catalog__body-container__filter__row">
                                    <div class="">
                                        <button class="btn catalog__body-container__filter__row__select2 " type="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            по цене
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-price">
                                            <div class="row">
                                                <div class="col-sm-2 px-0">
                                                    <p>От</p>
                                                </div>
                                                <div class="col-sm-10 px-0">
                                                    <input type="text">
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2 px-0">
                                                    <p>До</p>
                                                </div>
                                                <div class="col-sm-10 px-0">
                                                    <input type="text">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <button class="btn catalog__body-container__filter__row__select2 " type="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            по цвету
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-color">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                        for($i = 1; $i < 13; $i++){
                                                            require("catalogColorCircleFilter.component.php");
                                                        }
                                                        ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <select class="catalog__body-container__filter__row__select js-select--size"
                                        name="state" id="size">
                                    </select>
                                </div>
                                <div class="catalog__body-container__filter__row advance-field-target"
                                    id="advance-field">

                                    <select class="catalog__body-container__filter__row__select js-select--material"
                                        name="state" id="material">
                                    </select>
                                    <select class="catalog__body-container__filter__row__select js-select--detail"
                                        name="state" id="detail">
                                    </select>
                                    <select class="catalog__body-container__filter__row__select js-select--style"
                                        name="state" id="style">
                                    </select>
                                    <select class="catalog__body-container__filter__row__select js-select--decore"
                                        name="state" id="decore">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="catalog__body-container__filter__order">
                                    <select class="catalog__body-container__filter__order__select" id="order">
                                    </select>

                                    <img src="./images/plus-filter.png" alt="plus-filter.png" id="advance-search">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="catalog__body-container__display">
                        <div class="row">
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                            <?php require("catalog-item.php"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once("footer.php"); ?>