<?php require_once("header.php"); ?>
<?php require_once("breadCrumbs.php"); ?>
<div class="articles">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="articles__title">
                    Справочник
                </h3>
            </div>
        </div>
        <div class="row" id="NodeParent">
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-1.png" alt="articles-1">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-2.png" alt="articles-2">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-3.png" alt="articles-3">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-4.png" alt="articles-4">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-5.png" alt="articles-5">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-6.png" alt="articles-6">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-no--image.png" alt="articles-7">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="articles__item">
                    <a href="">
                        <img src="images/articles-no--image.png" alt="articles-8">
                    </a>
                    <span>12 декабря 2018</span>
                    <a href="">Как ухаживать за кашемиром?</a>
                    <span>Спойлер: крайне непросто</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 show-more">
                <form action="">
                    <a href="#" id="pullItems" value="submit">Показать еще</a>
                </form>
            </div>
        </div>
    </div>
</div>


<?php require_once("slider.php"); ?>
<?php require_once("footer.php"); ?>